let express = require('express');
let router = express.Router();
let DeliveryController = require('../controllers/DeliveryController');

router.get('/delivery-info/:id', DeliveryController.getDelivery);
router.put('/delivery-address/:id', DeliveryController.changeDeliveryAddress);
router.put('/delivery-date/:id', DeliveryController.changeDeliveryDate);
router.put('/delivery-note/:id', DeliveryController.changeDeliveryNote);
router.post('/auth/smsRequest', DeliveryController.otpRequest);
router.post('/auth/smsCheck', DeliveryController.otpCheck);
module.exports = router;
