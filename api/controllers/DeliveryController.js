let request = require('request-promise');
let config = require('../config');

const DeliveryController = {
  getDelivery: (req, res, next) => {
    let deliveryId = req.params.id;

    request({
      uri: config.apiUrl + `/exposed-api/delivery/${deliveryId}`,
      headers: {
        'content-type': 'application/json'
      },
      transform: (body, response) => {
        if (response.headers['content-type'] && response.headers['content-type'].includes('application/json')) {
          return JSON.parse(body);
        }
      }
    })
      .then((data) => {
        res.status(200).send(data)
      })
      .catch((err) => {
        res.status(500).send(err)
      });
  },
  changeDeliveryAddress: (req, res, next) => {
    let deliveryId = req.params.id;
    request({
      method: 'PUT',
      body: Object.assign({}, req.body),
      uri: config.apiUrl + `/exposed-api/delivery-address/${deliveryId}`,
      headers: {
        'content-type': 'application/json'
      },
      json: true
    })
      .then((data) => {
        res.status(200).send(data.data ? data.data : null)
      })
      .catch((err) => {
        res.status(500).send(err)
      });
  },
  changeDeliveryDate: (req, res, next) => {
    let deliveryId = req.params.id;
    request({
      method: 'PUT',
      body: Object.assign({}, req.body),
      uri: config.apiUrl + `/exposed-api/delivery-date/${deliveryId}`,
      headers: {
        'content-type': 'application/json'
      },
      json: true
    })
      .then((data) => {
        res.status(200).send(data.data ? data.data : null)
      })
      .catch((err) => {
        res.status(500).send(err)
      });
  },
  changeDeliveryNote: (req, res, next) => {
    let deliveryId = req.params.id;
    request({
      method: 'PUT',
      body: Object.assign({}, req.body),
      uri: config.apiUrl + `/exposed-api/delivery-note/${deliveryId}`,
      headers: {
        'content-type': 'application/json'
      },
      json: true
    })
      .then((data) => {
        res.status(200).send(data.data ? data.data : null)
      })
      .catch((err) => {
        res.status(500).send(err)
      });
  },
  otpRequest: (req, res, next) => {
    request({
      method: 'POST',
      body: Object.assign({}, req.body),
      uri: config.apiUrl + `/v1/auth/otpRequest`,
      headers: {
        'content-type': 'application/json'
      },
      json: true
    })
      .then((data) => {
        res.status(200).send(data ? data : null)
      })
      .catch((err) => {
        res.status(500).send(err)
      });
  },
  otpCheck: (req, res, next) => {
    request({
      method: 'POST',
      body: Object.assign({}, req.body),
      uri: config.apiUrl + `/v1/auth/otpCheck`,
      headers: {
        'content-type': 'application/json'
      },
      json: true
    })
      .then((data) => {
        res.status(200).send(data ? data : null)
      })
      .catch((err) => {
        res.status(500).send(err)
      });
  },
};

module.exports = DeliveryController;
