To start the server on localhost please use 
npm run start-dev

To start the server on production please use 
sudo pm2 start tracking.sh --name tracking

where tracking.sh contains 2 lines:
cd /home/ubuntu/tracking/
sudo npm start
