export const environment = {
  production: true,
  apiUrl: 'https://api.nemlevering.dk/api',
  ratingUrl: 'http://rating.nemlevering.dk'
};
