import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "../common/services/common.service";
import { LanguageService } from "../common/services/language.service";
import { UtilityService } from "../common/services/utility.service";
import * as moment from 'moment';
import { DragScrollComponent } from 'ngx-drag-scroll/lib';

import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'reschedule',
  templateUrl: './reschedule.component.html',
  styleUrls: ['./reschedule.component.scss']
})
export class RescheduleComponent implements OnInit {
  @ViewChild ('nav', {read: DragScrollComponent}) ds: DragScrollComponent;

  lang: string;
  id: string;
  timeFrom: any = { "hour": 0, "minute": 0 };
  timeTo: any = { "hour": 0, "minute": 0 };
  delivery: any;
  dateBlocks: any;
  timeBlocks: any;
  disabledActions: boolean = true;
  currentWeekNumber: number;

  constructor(private activatedRoute: ActivatedRoute,
    private commonService: CommonService,
    private router: Router,
    private modalService: NgbModal,
    private languageService: LanguageService,
    private utilityService: UtilityService) {
  }

  getStatus = (status) => this.utilityService.getStatus(status);
  getEventMessage = (event) => this.utilityService.getEventMessage(event);

  ngOnInit(): void {
    this.lang = this.activatedRoute.snapshot.params['language'];
    this.id = this.activatedRoute.snapshot.params['id'];
    this.commonService.getDeliveryInfo(this.id)
      .subscribe((resp: any) => {
        if (!resp || !resp.success) return;

        this.delivery = resp.data;
        console.log("this.delivery===>", this.delivery)
        if (this.delivery && this.delivery.deliverywindowstart) {
          let timeFrom = this.delivery.deliverywindowstart.split(":");
          this.timeFrom.hour = timeFrom[0];
          this.timeFrom.minute = timeFrom[1];
          let timeTo = this.delivery.deliverywindowend.split(":");
          this.timeTo.hour = timeTo[0];
          this.timeTo.minute = timeTo[1];  
        }
        
        this.setDateBlocks();
        this.setTimeBlocks();
      });
  }

  getLanguageText = (val) => this.languageService.getText(val, this.lang);

  // setDateBlocks() {
  //   let dateBlocks = [];
  //   let deliverydate = moment(this.delivery.deliverydate);

  //   function getDayName(date, key) {
  //     let dayName;
  //     switch (key) {
  //       case 0:
  //         dayName = "Today";
  //         break;
  //       case 1:
  //         dayName = "Tomorrow";
  //         break;
  //       default:
  //         dayName = moment(date).format("dddd");
  //         break;
  //     }
  //     return dayName;
  //   }

  //   function getWeekNumber(date : Date) {
  //     var d = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
  //     var dayNum = d.getUTCDay() || 7;
  //     d.setUTCDate(d.getUTCDate() + 4 - dayNum);
  //     var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
  //     return Math.ceil((((d.valueOf() - yearStart.valueOf()) / 86400000) + 1) / 7)
  //   }

  //   var dateWeek = new Date();
  //   console.log("current week number", getWeekNumber(dateWeek))
  //   for (let i = 0; i <= 6; i++) {
  //     let date = new Date();
  //     if (i > 0) {
  //       date.setDate(date.getDate() + i);
  //     }
  //     let active = deliverydate.startOf('day').diff(moment(date).startOf('day'), 'days') === 0;

  //     dateBlocks.push({
  //       id: i,
  //       date: date,
  //       dayName: getDayName(date, i),
  //       monthName: moment(date).format("MMM"),
  //       day: date.getDate(),
  //       active: active,
  //       currentActive: active,
  //     })
  //   }
  //   this.dateBlocks = dateBlocks;
  // }
  getWeek(tmpDate: Date, start) {
    //Calcing the starting point
    var today = new Date(tmpDate.setHours(0, 0, 0, 0));
    var day = today.getDay() - start;
    var date = today.getDate() - day;
    // Grabbing Start/End Dates
    var StartDate = new Date(today.setDate(date));
    var EndDate = new Date(today.setDate(date + 6));
    return StartDate;
  }

  getDayName(date, key) {
    let dayName;
    switch (key) {
      case 0:
        dayName = "Today";
        break;
      case 1:
        dayName = "Tomorrow";
        break;
      default:
        dayName = moment(date).format("dddd");
        break;
    }
    return dayName;
  };

  getWeekNumber(date: Date) {
    var d = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
    var dayNum = d.getUTCDay() || 7;
    d.setUTCDate(d.getUTCDate() + 4 - dayNum);
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    return Math.ceil((((d.valueOf() - yearStart.valueOf()) / 86400000) + 1) / 7)
  };

  setDateBlocks() {
    let dateBlocks = [];
    let deliverydate = moment(this.delivery.deliverydate);

    var dateWeek = new Date();
    this.currentWeekNumber = this.getWeekNumber(dateWeek);
    console.log("current week number", this.getWeekNumber(dateWeek))
    for (let i = 0; i <= 6; i++) {
      var date = this.getWeek(dateWeek, i);
      let active = deliverydate.startOf('day').diff(moment(date).startOf('day'), 'days') === 0;

      dateBlocks.push({
        id: i,
        date: date,
        dayName: moment(date).format("dddd"),
        monthName: moment(date).format("MMM"),
        day: date.getDate(),
        active: active,
        currentActive: active,
      })
    }
    this.dateBlocks = dateBlocks;
  }

  setTimeBlocks() {
    let timeBlocks = [];
    let dateTimeHour = parseInt(this.timeFrom.hour);
    console.log("deliverydate", dateTimeHour)

    // var dateTime = new Date();
    // var dateTimeHour = dateTime.getHours();
    
    for (let i = 0; i < 12; i++) {

      let active = (dateTimeHour >= i*2+1 && dateTimeHour < i*2+3) ? true : false;
      let tmpFromTimeHour = i*2 + 1;
      let tmpToTimeHour = i*2 + 3;
      let showFromHour = (tmpFromTimeHour < 10) ? ('0' + tmpFromTimeHour) : (tmpFromTimeHour);
      let showToHour = (tmpToTimeHour < 10) ? ('0' + tmpToTimeHour) : (tmpToTimeHour);
      
      timeBlocks.push({
        id: i,
        fromTimeHour: showFromHour,  
        fromTimeMin: '00',
        toTimeHour: showToHour,
        toTimeMin: '00',
        active: active,
        currentActive: active,
      })
    }
    this.timeBlocks = timeBlocks;
    console.log("timeBlock", this.timeBlocks)
  }

  setActiveDate(id: number) {
    this.dateBlocks = this.dateBlocks.map(item => {
      item.active = false;
      return item;
    });
    let foundBlock = this.dateBlocks.find(item => item.id === id);
    if (foundBlock) {
      foundBlock.active = true;
      this.disabledActions = false;
    }
  }

  setActiveTime(id: number) {
    this.timeBlocks = this.timeBlocks.map(item => {
      item.active = false;
      return item;
    });
    let foundBlock = this.timeBlocks.find(item => item.id === id);
    if (foundBlock) {
      foundBlock.active = true;
      //this.disabledActions = false;
    }
  }

  goBack() {
    this.router.navigate([`/${this.lang}/${this.id}`]);
  }

  getDiffTimesBetween(date1, date2) {
    //Get 1 minute in milliseconds
    var oneMinutes = 1000 * 60;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    return Math.round(difference_ms / oneMinutes);
  }

  validationTime() {
    let today = moment(new Date());
    let deliverydate = moment(this.delivery.deliverydate);
    let diffValue = deliverydate.diff(today, 'days');

    let query = {};
    let newDate = this.dateBlocks.find(item => {
      return item.active && !item.currentActive
    });

    let deliveDate = new Date(this.delivery.deliverydate);
    let originDeliveryDays = deliveDate.getFullYear() + '-' + (deliveDate.getMonth() + 1) + '-' + deliveDate.getDate();
    let originFromDelivery = originDeliveryDays + ' ' + this.delivery.deliverywindowstart;
    let originToDelivery = originDeliveryDays + ' ' + this.delivery.deliverywindowend;
    let date = new Date();
    let originFromDeliveryDate = new Date(originFromDelivery);
    let originToDeliveryDate = new Date(originToDelivery);

    let newDeliveryDate = originDeliveryDays;

    if (newDate) {
      query["deliverydate"] = newDate.date;
      let tmpDate = newDate.date.getFullYear() + '-' + (newDate.date.getMonth() + 1) + '-' + newDate.date.getDate();
      newDeliveryDate = tmpDate;
    }

    let newFromDelivery = newDeliveryDate + ' ' + this.timeFrom.hour + ':' + this.timeFrom.minute;
    let newToDelivery = newDeliveryDate + ' ' + this.timeTo.hour + ':' + this.timeTo.minute;

    if (this.delivery.carrier && (this.delivery.truck && this.delivery.truck['_id'])) {
      alert("You can't change date and time. This delivery is already assigned to the other driver.");
      return false;
    }

    if (this.delivery.status != 1) {
      alert("You can't change date and time because you are already on the way to this delivery");
      return false;
    }
    else {
      let newFromDelivDays = new Date(newFromDelivery);
      let newToDelivDays = new Date(newToDelivery);

      let diffStart = this.getDiffTimesBetween(originFromDeliveryDate, newFromDelivDays);
      let diffEnd = this.getDiffTimesBetween(originToDeliveryDate, newToDelivDays);

      let diffCurFrom = this.getDiffTimesBetween(date, originFromDeliveryDate);
      let diffCurNewFrom = this.getDiffTimesBetween(date, newFromDelivDays);

      console.log("diffCurFrom", diffCurFrom)
      console.log("diffCurNewFrom", diffCurNewFrom)
      console.log("diffStart", diffStart)
      console.log("diffEnd", diffEnd)

      // if (diffCurFrom < 0 || diffCurNewFrom < 0) {
      //   alert("You can't change date and time. Now you are in the delivery window time.");
      //   return false;
      // }

      if ((diffStart >= 0 && diffEnd <= 0)) {

        if (diffCurFrom < 5) {
          alert("You can't change date and time because delivery start time is less than 5 minutes before delivery.");
          return false;
        }
        if (diffCurFrom < 150 && diffCurNewFrom > (14 * 60)) {
          alert("You can't change date and time. The location where driver is expected is less than 20 Km but the change in delivery time is more than 14 hours");
          return false;
        }
        else if (diffCurFrom < 20 && diffCurNewFrom > 60) {
          alert("You can't change date and time. The location where driver is expected is less than 3 Km but the change in delivery time is more than 1 hours");
          return false;
        }
      }
    }
    return true;
  }

  reschedule(modalRef: NgbModalRef) {
    let today = moment(new Date());
    let deliverydate = moment(this.delivery.deliverydate);
    let diffValue = deliverydate.diff(today, 'days');
    // if (diffValue === 0 || (diffValue === 1 && parseInt(today.format("H")) < 13)) {
    //   alert("You can't change date and time for today's or tomorrow's delivery");
    //   return;
    // }

    let query = {};
    let newDate = this.dateBlocks.find(item => {
      return item.active && !item.currentActive
    });

    if (newDate) {
      query["deliverydate"] = newDate.date;
    }

    let newTime = this.timeBlocks.find(item => {
      return item.active && !item.currentActive
    });

    if (newTime) {
      this.timeFrom.hour = parseInt(newTime.fromTimeHour);
      this.timeFrom.minute = 0;
      this.timeTo.hour = parseInt(newTime.toTimeHour);
      this.timeTo.minute = 0;
    }

    console.log("newDate.date", newDate.date);
    console.log("newTime", newTime);

    if (!this.validationTime()) {
      return;
    }

    query["deliverywindowstart"] = (this.timeFrom.hour.toString().length < 2 ? "0" + this.timeFrom.hour : this.timeFrom.hour) + ":" + (this.timeFrom.minute.toString().length < 2 ? "0" + this.timeFrom.minute : this.timeFrom.minute);
    query["deliverywindowend"] = (this.timeTo.hour.toString().length < 2 ? "0" + this.timeTo.hour : this.timeTo.hour) + ":" + (this.timeTo.minute.toString().length < 2 ? "0" + this.timeTo.minute : this.timeTo.minute);

    this.commonService.putDeliveryDate(this.id, query).subscribe((res: any) => {
      if (res.success) {
        if (res.data.deliverydate) {
          this.delivery.deliverydate = res.data.deliverydate;
        }
        this.delivery.deliverywindowstart = res.data.deliverywindowstart;
        this.delivery.deliverywindowend = res.data.deliverywindowend;
        this.modalService.open(modalRef);
      }
    });
  }

  goTrack() {
    this.router.navigate([`/${this.lang}/${this.id}`]);
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }

  w2date(year, wn, dayNb){
    var j10 = new Date( year,0,10,12,0,0),
        j4 = new Date( year,0,4,12,0,0),
        mon1 = j4.getTime() - j10.getDay() * 86400000;
    return new Date(mon1 + ((wn - 1)  * 7  + dayNb) * 86400000);
  };

  nextWeek() {
    let dateBlocks = [];
    let deliverydate = moment(this.delivery.deliverydate);
    
    var date = new Date();
    
    this.currentWeekNumber = this.currentWeekNumber + 1;
    if (this.currentWeekNumber > 53) {
      this.currentWeekNumber = 1;
      var dateWeek = this.w2date(date.getFullYear()+1, this.currentWeekNumber, 0);
    }
    else {
      var dateWeek = this.w2date(date.getFullYear(), this.currentWeekNumber, 0);
    }
    
    console.log("current week number", this.getWeekNumber(dateWeek))

    for (let i = 0; i <= 6; i++) {
      var date = this.getWeek(dateWeek, i);
      let active = deliverydate.startOf('day').diff(moment(date).startOf('day'), 'days') === 0;

      dateBlocks.push({
        id: i,
        date: date,
        dayName: moment(date).format("dddd"),
        monthName: moment(date).format("MMM"),
        day: date.getDate(),
        active: active,
        currentActive: active,
      })
    }
    this.dateBlocks = dateBlocks;
  }

  prevWeek() {
    let dateBlocks = [];
    let deliverydate = moment(this.delivery.deliverydate);
    
    var date = new Date();
    
    this.currentWeekNumber = this.currentWeekNumber - 1;
    if (this.currentWeekNumber < 0) {
      this.currentWeekNumber = 53;
      var dateWeek = this.w2date(date.getFullYear()-1, this.currentWeekNumber, 0);
    }
    else {
      var dateWeek = this.w2date(date.getFullYear(), this.currentWeekNumber, 0);
    }

    console.log("current week number", this.getWeekNumber(dateWeek))
    
    for (let i = 0; i <= 6; i++) {
      var date = this.getWeek(dateWeek, i);
      let active = deliverydate.startOf('day').diff(moment(date).startOf('day'), 'days') === 0;

      dateBlocks.push({
        id: i,
        date: date,
        dayName: moment(date).format("dddd"),
        monthName: moment(date).format("MMM"),
        day: date.getDate(),
        active: active,
        currentActive: active,
      })
    }
    this.dateBlocks = dateBlocks;
  }
}
