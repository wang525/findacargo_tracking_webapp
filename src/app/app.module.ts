import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { DragScrollModule } from 'ngx-drag-scroll';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import {CommonService} from "./common/services/common.service";
import {LanguageService} from "./common/services/language.service";
import {UtilityService} from "./common/services/utility.service";
import {DateTimePipe} from "./common/pipes/date-time.pipe";

import {environment} from "../environments/environment";
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AgmCoreModule } from '@agm/core';
import {RescheduleComponent} from "./reschedule/reschedule.component";

const config: SocketIoConfig = { url: environment.apiUrl, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    RescheduleComponent,
    DateTimePipe
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyADQx4-dsxmzj6jP1pH__Wb6xl9nYdO2Es",
      libraries: ["places", "geometry"]
    }),
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FormsModule,
    DragScrollModule,
    NgbModule.forRoot(),
    SocketIoModule.forRoot(config)
  ],
  providers: [CommonService, LanguageService, UtilityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
