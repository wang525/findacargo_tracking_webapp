import { Component, ElementRef, EventEmitter, NgZone, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "../common/services/common.service";
import { LanguageService } from "../common/services/language.service";
import { UtilityService } from "../common/services/utility.service";
import { environment } from "../../environments/environment";
import { Socket } from "ng-socket-io";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { MapsAPILoader } from "@agm/core";
import {} from '@types/googlemaps';
import * as moment from 'moment';
import { start } from "repl";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  mapInitializeEmitter: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('routemap', { read: ElementRef }) map: ElementRef;
  mapError: String = "";
  lang: string;
  id: string;
  directionsService: any;
  directionsDisplay: any;
  truckMarker: any;
  mapOptions: any;
  events: any;
  delivery: any;
  destination: string;
  user_input_number: string;
  delivery_note: string;
  updateTruckPosition: boolean;
  changedTempAddress = null;
  modalRef: NgbModalRef;
  confirmModalRef: NgbModalRef;
  modalChangedView = false;
  supportEmail: string = "hej@goidag.com";
  supportPhone: string = "+45 787 54 758";
  modalContent: any;
  reschedule_button: boolean;
  delivery_note_applyall = false;

  googleMap: any = null;

  set30min: boolean;       /* set before 30 min */
  showClaim = [];
  smsConfirmed: boolean = false;
  smsRequested: boolean = false;
  smsNumber: string = '';
  checkedPhoneNumber: boolean = false;

  constructor(private activatedRoute: ActivatedRoute,
    private commonService: CommonService,
    private socket: Socket,
    private modalService: NgbModal,
    private languageService: LanguageService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private router: Router,
    private utilityService: UtilityService) {
    this.setTimeIntervalFunc();

    this.smsConfirmed = this.commonService.smsConfirmed;
    this.showClaim = [
      '',
      this.getLanguageText("report_damaged_box"),
      this.getLanguageText("client_does_not_answer"),
      this.getLanguageText("missing_box"),
      this.getLanguageText("another_report"),
    ];
  }

  getStatus = (status) => this.utilityService.getStatus(status);
  getEventMessage = (event) => this.utilityService.getEventMessage(event);

  ngOnInit(): void {
    console.log("ngInit")
    this.checkedPhoneNumber = false;
    this.lang = this.activatedRoute.snapshot.params['language'];
    this.id = this.activatedRoute.snapshot.params['id'];
    this.commonService.getDeliveryInfo(this.id)
      .subscribe((resp: any) => {
        if (!resp || !resp.success) return;

        console.log(resp.data)
        this.delivery = resp.data;
        this.delivery_note = this.delivery.deliverynotes
        /*Test codes  for SMS begin*/
        /*this.delivery.client.phone = '004530143748'
        this.delivery.allow_to_reschedule = true;*/
        /**Test codes for SMS end*/
        this.delivery.isCarrirAssigned = this.delivery != 1 ? true : false;
        this.setEvents(this.delivery.events);
        this.setDestination();
        this.mapInitializeEmitter.emit(true);
        // this.reschedule_button = this.delivery.status != 0 && this.delivery.status != 3 && this.delivery.allow_to_reschedule;
        this.reschedule_button = true;
        

        /* delviery window start time fix */
        var startTime = this.delivery.deliverywindowstart;
        var endTime = this.delivery.deliverywindowend;
        
        console.log("startTime", startTime);
        console.log("endTime", endTime);
        
        if (this.delivery.deliverywindowstart) {
          if (startTime.includes(".")) {
            var startSplitTime = startTime.split(".");
          }
          else if (startTime.includes(":")) {
            var startSplitTime = startTime.split(":");
          }
          if (endTime.includes(".")) {
            var endSplitTime = endTime.split(".");
          }
          else if (endTime.includes(":")) {
            var endSplitTime = endTime.split(":");
          }
          this.delivery.deliverywindowstart = startSplitTime[0] + ":" + startSplitTime[1];
          this.delivery.deliverywindowend = endSplitTime[0] + ":" + endSplitTime[1];
        }

        let date = new Date();
        //console.log("date", date);
        this.calculateDiffTime(date);

        this.socket.on('delivery_status_changed_' + this.id, (data) => {
          // console.log('delivery_status_changed_', data)
          this.commonService.getDeliveryInfo(this.id)
            .subscribe((resp: any) => {
              if (!resp || !resp.success) return;

              this.delivery = resp.data;
              this.delivery_note = this.delivery.deliverynotes
              this.delivery.isCarrirAssigned = this.delivery != 1 ? true : false;
              this.setEvents(this.delivery.events);
              this.setDestination();
              this.mapInitializeEmitter.emit(true);
              // this.reschedule_button = this.delivery.status != 0 && this.delivery.status != 3 && this.delivery.allow_to_reschedule;
              this.reschedule_button = true;
            });
        });
      });

  }

  getDiffTimesBetween(date1, date2) {
    //Get 1 minute in milliseconds
    var oneMinutes = 1000 * 60;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    return Math.round(difference_ms / oneMinutes);
  }

  /* calcualte the time difference between delivery time and current time */
  calculateDiffTime(currentTime) {
    if (this.delivery && this.delivery.deliverywindowstart) {
      let deliveDate = new Date(this.delivery.deliverydate);

      let deliveryYear = deliveDate.getFullYear();
      let deliveryMonth = deliveDate.getMonth() + 1;
      let deliveryDay = deliveDate.getDate();


      let deliveryDate = deliveryYear + '-' + deliveryMonth + '-' + deliveryDay + ' ' + this.delivery.deliverywindowstart;


      let deliveryStartDateTime = new Date(deliveryDate);
      let diff = this.getDiffTimesBetween(currentTime, deliveryStartDateTime);

      console.log("diff", diff);
      if (diff >= 30) {
        this.set30min = true;
      }
      else {
        this.set30min = false;
      }
    }
    return this.set30min;
  }

  /* time setting */
  setTimeIntervalFunc() {
    let cnt = 0;

    setInterval(() => {
      let date = new Date();

      this.calculateDiffTime(date);
    }, 10000)
  }


  /* before 30 min to start */
  isStart30min() {
    return this.set30min;
  }

  setEvents(events) {
    this.events = events.sort((a, b) => {
      a = new Date(a.created_at);
      b = new Date(b.created_at);
      return a > b ? -1 : a < b ? 1 : 0;
    });
  }

  setDestination() {
    this.destination = this.delivery.deliveryaddress + ", " + this.delivery.deliveryzip + ", " + this.delivery.deliverycity;
  }

  ngAfterViewInit() {
    this.mapInitializeEmitter.subscribe(() => {
      this.initializeMap();
    });
  }

  reschedule() {
    this.router.navigate([`/${this.lang}/reschedule/${this.id}`]);
  }

  getLanguageText = (val) => this.languageService.getText(val, this.lang);

  initializeMap = () => {
    if (!this.map.nativeElement) return false;
    this.mapsAPILoader.load().then(() => {

      let pointA = {
        lng: parseFloat(this.delivery.pickupcoordinates[0]),
        lat: parseFloat(this.delivery.pickupcoordinates[1])
      },
        pointB = {
          lng: parseFloat(this.delivery.deliverycoordinates[0]),
          lat: parseFloat(this.delivery.deliverycoordinates[1])
        };
      let map = new google.maps.Map(this.map.nativeElement, {
        zoom: 10,
        center: pointA
      });

      this.googleMap = map;

      this.updateTruckPosition = this.delivery.truck
        && this.delivery.truck.liveDelivery
        && this.delivery.truck.liveDelivery.location
        && this.delivery.status == 2;

      this.directionsService = new google.maps.DirectionsService;

      if (this.updateTruckPosition) {
        this.directionsDisplay = new google.maps.DirectionsRenderer({
          map: map,
          suppressMarkers: false,
          preserveViewport: true
        });  
      }
      else {
        this.directionsDisplay = new google.maps.DirectionsRenderer({
          map: map,
          suppressMarkers: false
        });
      }
      
      this.mapOptions = {
        origin: pointA,
        destination: pointB,
        avoidTolls: true,
        avoidHighways: false,
        travelMode: google.maps.TravelMode.DRIVING
      };
      this.displayRoute();

      let waypoint = null;


      /**Test codes  for truck marker START */
      // let deliverycoordinates = this.delivery.deliverycoordinates;
      // let truckPoint = {lng: parseFloat(deliverycoordinates[0]), lat: parseFloat(deliverycoordinates[1])};
      // let showLabel = "";

      // if (!this.delivery.delivered_at && this.delivery.estimated_delivery_time) {

      //   showLabel = this.delivery.estimated_delivery_time;
      // }
      // else {
      //   showLabel = this.delivery.deliverywindowend;
      // }

      // console.log("showLabel", showLabel)
      //   this.truckMarker = new google.maps.Marker({
      //       position: truckPoint,
      //       icon:'assets/images/truck.png',
      //       map: map,
      //       label: {
      //         text: showLabel,
      //         color: "#ffffff",
      //         fontSize: "13px",
      //       }
      //   });
      /**Test codes  for truck marker END */

      if (this.updateTruckPosition) {
        let coordinates = this.delivery.truck.liveDelivery.location.coordinates;
        let tracks = this.delivery.truck.liveDelivery.location.tracks;

        let truckPoint = null;

        if (tracks && tracks.length > 0) {
          //For new track model structure
          let wayPoints = [];
          truckPoint = { lng: parseFloat(tracks[tracks.length - 1].lng), lat: parseFloat(tracks[tracks.length - 1].lat) };

          tracks.map(track => {
            wayPoints.push({
              location: new google.maps.LatLng(parseFloat(track.lat), parseFloat(track.lng)),
              stopover: false
            })
          })
          this.mapOptions.waypoints = wayPoints;
          this.mapOptions.optimizeWaypoints = true;
        }
        else if (coordinates && coordinates.length > 0) {
          //For still working with legacy track model
          truckPoint = { lng: parseFloat(coordinates[0]), lat: parseFloat(coordinates[1]) };
          waypoint = new google.maps.LatLng(parseFloat(coordinates[1]), parseFloat(coordinates[0]));
          this.mapOptions.waypoints = [{ location: waypoint, stopover: false }];
          this.mapOptions.optimizeWaypoints = true;
        }

        if (truckPoint) {
          /* the current position of the car START */
          let showLabel = "";

          if (!this.delivery.delivered_at && this.delivery.estimated_delivery_time) {
            showLabel = this.delivery.estimated_delivery_time;
          }
          else {
            showLabel = this.delivery.deliverywindowend;
          }

          this.truckMarker = new google.maps.Marker({
            position: truckPoint,
            icon: 'assets/images/truck.png',
            map: map,
            label: {
              text: showLabel,
              color: "#ffffff",
              fontSize: "13px",
            }
          });
          /* the current position of the car END */
        }
                
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(truckPoint);
        bounds.extend(pointB);
        this.googleMap.fitBounds(bounds);
        
        this.initSocket();
      }
    });
  };

  initSocket() {
    this.socket.on('vehicle_location_changed_' + this.delivery.truck._id, (data) => {
      var pointB = {
        lng: parseFloat(this.delivery.deliverycoordinates[0]),
        lat: parseFloat(this.delivery.deliverycoordinates[1])
      };

      let newPosition = new google.maps.LatLng(data.latitude, data.longitude);
      if (this.truckMarker) {
        this.truckMarker.setPosition(newPosition);
      }
      else if (this.delivery.status == 2) {
        /* the current position of the car START */
        let truckPoint = { lng: parseFloat(data.longitude), lat: parseFloat(data.latitude) };
        let showLabel = "";

        if (!this.delivery.delivered_at && this.delivery.estimated_delivery_time) {
          showLabel = this.delivery.estimated_delivery_time;
        }
        else {
          showLabel = this.delivery.deliverywindowend;
        }

        this.truckMarker = new google.maps.Marker({
          position: truckPoint,
          icon: 'assets/images/truck.png',
          map: this.googleMap,
          label: {
            text: showLabel,
            color: "#ffffff",
            fontSize: "13px",
          }
        });

        var bounds = new google.maps.LatLngBounds();
        bounds.extend(truckPoint);
        bounds.extend(pointB);
        this.googleMap.fitBounds(bounds);
        /* the current position of the car END */
      }

      if (this.mapOptions.waypoints && this.mapOptions.waypoints.length > 0) {
        //Add new truck position to current track
        this.mapOptions.waypoints.push({ location: newPosition, stopover: false });
      }
      else {
        this.mapOptions.waypoints = [{ location: newPosition, stopover: false }];
      }

      this.displayRoute();

    });

  }

  displayRoute = () => {
    this.mapError = '';
    let that = this;
    this.directionsService.route(this.mapOptions, (response, status) => {
      if (status == google.maps.DirectionsStatus.OK) {

        that.directionsDisplay.setDirections(response);
      } else {
        //ignore current position and try again
        this.mapOptions.waypoints = []
        this.directionsService.route(this.mapOptions, (response, status) => {
          if (status == google.maps.DirectionsStatus.OK) {
            this.directionsDisplay.setDirections(response);
          } else {
            this.mapError = 'Error accured while calculating the route';
          }
        })
      }
    });
  };

  changeAddressOpen(content) {
    let autocomplete;
    this.modalChangedView = false;
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.changedTempAddress = null;
      this.setDestination();
    }, (reason) => {
      this.changedTempAddress = null;
      this.setDestination();
    });

    let element = <HTMLInputElement>document.getElementById('change-address-autocomplete');
    autocomplete = new google.maps.places.Autocomplete(element, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();

        function getGoogleComponent(type: string) {
          let component = place.address_components.find((item) => item.types.indexOf(type) > -1);
          return component ? component.long_name : null;
        }

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }

        this.changedTempAddress = {
          route: getGoogleComponent('route'),
          street_number: getGoogleComponent('street_number'),
          zip: getGoogleComponent('postal_code'),
          city: getGoogleComponent('locality'),
          country: getGoogleComponent('country'),
          latitude: place.geometry.location.lat(),
          longitude: place.geometry.location.lng(),
        };
      });
    });
  }

  confirmPhoneView(content1, content2) {
    if (this.delivery.client && this.delivery.client.phone && this.delivery.client.phone.length > 0) {
      this.modalChangedView = false;
      this.confirmModalRef = this.modalService.open(content1);
      this.modalContent = content2
    }
  }

  confirmPhoneSMSView() {
    if (!this.smsConfirmed) {
      // if (!this.smsRequested) {
        //Send sms request
        if (this.delivery.client && this.delivery.client.phone && this.delivery.client.phone.length > 0) {
          this.sendSmsRequest(this.delivery.client.phone)
          // this.smsRequested = false;
          this.modalChangedView = false;
        }
      // }
    }
    else {
      console.log("this.smsConfimed true =======")
      this.onConfirmPhone();
    }
  }

  confirmNoteView(content1, content2) {
    this.modalRef = this.modalService.open(content1);
    //this.modalContent = content2
  }

  sendSmsRequest(phone: string) {
    this.commonService.smsRequest(phone).subscribe((res: any) => {
      console.log('sendSmsRequest', res)
    });
  }

  confirmPhone() {
    if (this.delivery.client&&this.delivery.client.phone&&this.delivery.client.phone.length>0) {
      var phoneNumber = (this.delivery.client.phone).toString();
      if (this.user_input_number == phoneNumber.substr(phoneNumber.length - 6)) {
        this.checkedPhoneNumber = true;
        this.user_input_number = "";
        this.confirmPhoneSMSView();
      }
      else {
        alert(this.getLanguageText('incorrect_match'));
        console.log('confirmPhone-error');
        this.checkedPhoneNumber = false;
      }
    }
  }

  confirmPhoneSMS() {
    // for test
    // this.confirmModalRef.close();
    // this.onConfirmPhone();
    if (this.delivery.client&&this.delivery.client.phone&&this.delivery.client.phone.length>0) {
      this.commonService.smsCheck(this.delivery.client.phone, this.user_input_number).subscribe((res: any) => {
        if (res&&res.result&&res.result=='success') {
          console.log('confirm sms check ');
          this.confirmModalRef.close();
          this.onConfirmPhone();
        }
        else {
          alert(this.getLanguageText('incorrect_match'));
          console.log('confirm sms error');
        }
      });
    }
  }

  confirmNote() {
    // this.modalRef.close();
    this.commonService.putDeliveryNote(this.delivery._id, {deliverynotes: this.delivery_note, applyall: this.delivery_note_applyall }).subscribe((res: any) => {
      if (res.success) {
        this.modalRef.close();
      }
    });
  }

  onConfirmPhone() {
    this.smsConfirmed = true;
    this.commonService.smsConfirmed = true;
    console.log("this.modalContent", this.modalContent)
    if (this.modalContent == null) this.reschedule()
    else this.changeAddressOpen(this.modalContent)
  }

  validationAddress() {
    let today = moment(new Date());
    let deliverydate = moment(this.delivery.deliverydate);
    let diffValue = deliverydate.diff(today, 'days');

    let deliveDate = new Date(this.delivery.deliverydate);
    let originDeliveryDays = deliveDate.getFullYear() + '-' + (deliveDate.getMonth() + 1) + '-' + deliveDate.getDate();
    let originFromDelivery = originDeliveryDays + ' ' + this.delivery.deliverywindowstart;
    let originToDelivery = originDeliveryDays + ' ' + this.delivery.deliverywindowend;
    let date = new Date();
    let originFromDeliveryDate = new Date(originFromDelivery);

    let firstLatLng = new google.maps.LatLng(this.delivery.deliverycoordinates[1], this.delivery.deliverycoordinates[0]);
    let secondLatLng = new google.maps.LatLng(this.changedTempAddress.latitude, this.changedTempAddress.longitude);
    let distance = Math.ceil(google.maps.geometry.spherical.computeDistanceBetween(firstLatLng, secondLatLng));

    if (this.delivery.carrier) {
      alert("You can't change date and time. This delivery is already assigned to the other driver.");
      return false;
    }

    if (this.delivery.status != 1) {
      alert("You can't change date and time because you are already on the way to this delivery");
      return false;
    }
    else {
      let diffCurFrom = this.getDiffTimesBetween(date, originFromDeliveryDate);

      // if (diffCurFrom < 0) {
      //   alert("You can't change address. Now you are in the delivery window time.");
      //   return false;
      // }

      if (distance < 20000 && diffCurFrom > (14 * 60)) {
        alert("You can't change address. The location where driver is expected is less than 20 Km but delivery time is more than 14 hours");
        return false;
      }
      else if (distance < 5000 && diffCurFrom > 60) {
        alert("You can't change address. The location where driver is expected is less than 5 Km but delivery time is more than 1 hours");
        return false;
      }
      else if (distance < 3000 && diffCurFrom < 5) {
        alert("You can't change address. The location where driver is expected is less than 3 Km but delivery time is less than 5 minuts.");
        return false;
      }
    }
    return true;
  }

  changeAddress() {
    if (!this.changedTempAddress || !this.changedTempAddress.route || !this.changedTempAddress.street_number || !this.changedTempAddress.zip || !this.changedTempAddress.city) {
      alert("You have provided an invalid address");
      return;
    }

    // let deliveDate = new Date(this.delivery.deliverydate);
    // let originDeliveryDays = deliveDate.getFullYear() + '-' + (deliveDate.getMonth() + 1) + '-' + deliveDate.getDate();
    // let originFromDelivery = originDeliveryDays + ' '+ this.delivery.deliverywindowstart;
    // let originToDelivery = originDeliveryDays + ' '+ this.delivery.deliverywindowend;
    // let date = new Date();
    // let originFromDeliveryDate = new Date(originFromDelivery);
    // let originToDeliveryDate = new Date(originToDelivery);

    // let newDeliveryDate = originDeliveryDays;

    // if (this.delivery.status != 1) {
    //   alert("You can't change date and time because you are already on the way to this delivery");
    //   return;
    // }
    // else {
    //   let diff = this.getDiffTimesBetween(date, originFromDeliveryDate);
    //   console.log("diff", diff)
    //   if (diff < 5) {
    //     alert("You can't change date and time because delivery start time is less than 5 minutes before delivery.");
    //     return;
    //   }
    // }

    if (!this.validationAddress()) {
      return;
    }
    let today = moment(new Date());
    let deliverydate = moment(this.delivery.deliverydate);
    let diffValue = deliverydate.diff(today, 'days');
    if (diffValue === 0 || (diffValue === 1 && parseInt(today.format("H")) >= 13)) {
      let firstLatLng = new google.maps.LatLng(this.delivery.deliverycoordinates[1], this.delivery.deliverycoordinates[0]);
      let secondLatLng = new google.maps.LatLng(this.changedTempAddress.latitude, this.changedTempAddress.longitude);
      let distance = Math.ceil(google.maps.geometry.spherical.computeDistanceBetween(firstLatLng, secondLatLng));
      if (distance > 3000) {
        alert("You can set new delivery address only if it is < 3 km away");
        return;
      }
    }


    this.commonService.putDeliveryAddress(this.id, this.changedTempAddress).subscribe((res: any) => {
      if (res.success) {
        this.delivery.deliveryaddress = res.data.deliveryaddress;
        this.delivery.deliveryzip = res.data.deliveryzip;
        this.delivery.deliverycity = res.data.deliverycity;
        this.delivery.deliverycoordinates[0] = res.data.deliverycoordinates[0];
        this.delivery.deliverycoordinates[1] = res.data.deliverycoordinates[1];
        this.setDestination();
        this.modalChangedView = true;
        let pointA = {
          lng: parseFloat(this.delivery.pickupcoordinates[0]),
          lat: parseFloat(this.delivery.pickupcoordinates[1])
        },
          pointB = {
            lng: parseFloat(this.delivery.deliverycoordinates[0]),
            lat: parseFloat(this.delivery.deliverycoordinates[1])
          };
        this.mapOptions = {
          origin: pointA,
          destination: pointB,
          avoidTolls: true,
          avoidHighways: false,
          travelMode: google.maps.TravelMode.DRIVING
        };
        this.displayRoute();

        if (res.event) {
          this.delivery.events.push(res.event);
          this.setEvents(this.delivery.events);
        }
      }
      setTimeout(() => {
        this.modalChangedView = false;
        this.modalRef.close();
      }, 3000);
    })
  }

  // confirmTime(value) {
  //   let arrVal = value.split('.');
  //   if (arrVal.length > 1) {
  //     let arrMin = arrVal[1].split(':');
  //     console.log("arrMin", arrMin)
  //     if (arrMin.length > 0) {
  //       return arrVal[0] + ':' + arrMin[0];
  //     }
  //     return arrVal[0] + ':' + arrVal[1];
  //   }
  //   else {
  //     return value;
  //   }
  // }

  gotoRatingFeedback(id) {
    //console.log("environment.ratingUrl", environment.ratingUrl)
    window.open(environment.ratingUrl + '/' + this.lang + '/' + id, '_blank')
  }
}
