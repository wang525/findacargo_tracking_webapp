import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../../environments/environment";
import {tap} from "rxjs/operators";

@Injectable()
export class CommonService {

  public smsConfirmed:boolean = false;

  constructor(private http: HttpClient) {
  }

  getDeliveryInfo(id: string) {
    return this.http.get('/api/delivery-info/' + id)
      .pipe(
        tap(data => data)
      );
  }

  putDeliveryAddress(id: string, data: any) {
    return this.http.put('/api/delivery-address/' + id, data)
      .pipe(
        tap(data => data)
      );
  }

  putDeliveryDate(id: string, data: any) {
    return this.http.put('/api/delivery-date/' + id, data)
      .pipe(
        tap(data => data)
      );
  }

  putDeliveryNote(id: string, data: any) {
    return this.http.put('/api/delivery-note/' + id, data)
      .pipe(
        tap(data => data)
      );
  }

  smsRequest(phone: string) {
    return this.http.post('/api/auth/smsRequest', {phone:phone})
      .pipe(
        tap(data => data)
      );
  }

  smsCheck(phone: string, otpNumber:string) {
    return this.http.post('/api/auth/smsCheck', {phone:phone, otpNumber:otpNumber})
      .pipe(
        tap(data => data)
      );
  }

}
