import {Injectable} from '@angular/core';
import {LanguageConfig} from "../../language-config";

@Injectable()
export class LanguageService {
  constructor() {
  }

  getText(value: string, lang: string) {
    if (LanguageConfig.hasOwnProperty(value) && LanguageConfig[value].hasOwnProperty(lang)) {
      return LanguageConfig[value][lang];
    } else {
      return '';
    }
  }
}
