import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../../environments/environment";
import {tap} from "rxjs/operators";

@Injectable()
export class UtilityService {

  deliveryStatuses = [
    {'name': 'Created', 'description': 'Order Created', 'langKey':'order_created'},
    {'name': 'In progress', 'description': 'Delivery on the way', 'langKey':'in_progress'},
    {'name': 'Delivered', 'description': 'Delivered', 'langKey':'delivered'},
    {'name': 'Not Received', 'description': 'Not Received', 'langKey':'notreceived'},
    {'name': 'For re-delivery', 'description': 'For re-delivery', 'langKey':'redelivery'}
  ];

  constructor(private http: HttpClient) {
  }

  getEventMessage(event: any) {
    if (event.event == 'STATUS_UPDATE') {
      return this.deliveryStatuses[event.event_data - 1].name;
    } else {
      return event.event_data;
    }
  };

  getStatus(status: number) {
    return this.deliveryStatuses[status-1] ? this.deliveryStatuses[status-1] : null;
  };
}
