import {NgModule} from '@angular/core';
import {RouterModule, Routes, ActivatedRoute} from '@angular/router';
import {AppComponent} from './app.component';
import {MainComponent} from './main/main.component';
import {RescheduleComponent} from "./reschedule/reschedule.component";

const routes: Routes = [

  {path: '', component: AppComponent},

  {path: ':id', redirectTo: 'dan/:id', pathMatch: 'full'},
  {path: ':language/:id', component: MainComponent},

  {path: 'reschedule/:id', redirectTo: 'dan/reschedule/:id', pathMatch: 'full'},
  {path: ':language/reschedule/:id', component: RescheduleComponent},

];

@NgModule
({
  imports:
    [RouterModule.forRoot(routes, {useHash: false})],
  exports:
    [RouterModule]
})
export class AppRoutingModule {
}
